/*
	This head file is for the client side of the program.
	It will have the menu choice declarations, and
	prototypes
*/

#include <iostream>
#include <cctype>
#include <cstring>
#include "table.h"
#include "cs_field.h"

// MENU CHOICES //
const int ADD {1};
const int DISPLAY {2};
const int REMOVE {3};
const int RETRIEVE {4};
const int LOAD_FILE {5};
const int QUIT {9};

using namespace std;

//XXX Grader: These are allowed to be void per Karla's guidelines XXX //
void print_menu(int & menu_choice);
void get_table_size(int & table_size);
void add_field(table & hash_table);
void load_ex_file(table & hash_table);
void display_all(table & hash_table);
void retrieve(table & hash_table);
void remove(table & hash_table);
