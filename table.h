#include "cs_field.h"
#include <iostream>
//#include <fstream> // This is used for reading in the file
/*
	 Hayden Chalin
	 CS163
	 5-7-2024
	 This file will contain the class for the table ADT and member prototypes

 */
/*

	 XXX This is what the data looks like:
	 "Keyword|Description|List of three Aspects|Search History|something else\n"

 */
struct node {
	//char * data;
	cs_field field;
	node * next;
};

class table {

	public:
		table(int size); // make sure to test for 2 different sizes
		~table(void);
		int insert(char * key_value, const cs_field & to_add);
		int retrieve(char * key_value, cs_field & found);
		int display_all(void) const;
		int load(void);
		int remove(char * key_value);

	private:
		node ** hash_table;
		int table_size;
		int display_all(int & i, int & count)const;
		int hash(char * key_value); // call this in the insert function
		int remove(node * & head, char * key_value);		

};
