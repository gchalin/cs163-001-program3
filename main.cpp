#include "main.h"
//#include "table.h"
/*
	 Hayden Chalin
	 CS163
	 5-7-2024
	 This file represents the client facing program and all of its functionality
*/
int main(){

	int menu_choice {0};
	int table_size {0};

	get_table_size(table_size);
	// create hash table instance
	table hash_table(table_size);
	while (menu_choice != QUIT){

		print_menu(menu_choice);


		switch (menu_choice){
			case ADD:
				add_field(hash_table);
				break;
			case DISPLAY:
				display_all(hash_table);
				break;
			case REMOVE:
				remove(hash_table);
				break;
			case RETRIEVE:
				retrieve(hash_table);
				break;
			case LOAD_FILE:
				load_ex_file(hash_table);
				break;
			case QUIT:
				cout << "QUIT FN" << endl;
				break;
		}
	}
	return 1;
}

// Void return types are ok in the client program
void print_menu(int & menu_choice){
/*
	Display the menu and update the menu_choice
*/
	cout << "1)\t Add cs field" << endl;
	cout << "2)\t Display all fields" << endl;
	cout << "3)\t Remove a field" << endl;
	cout << "4)\t Retrieve a field" << endl;
	cout << "5)\t Load external file" << endl;
	cout << "9)\t Quit" << endl;

	// update menu choice
	cout << "Choice: ";
	cin >> menu_choice; cin.ignore(100, '\n');

};

void get_table_size(int & table_size){
/*
	This function will get the size of the hash table
	the user wants to create
*/
	cout << "Please enter the size: ";
	cin >> table_size; cin.ignore(100, '\n');


}

void add_field(table & hash_table){
/*
		This function will add a single cs_field to the hash table
*/

	// create cs_field instance
	cs_field field;

	char title[50];
	char desc[50];
	char aspects[50];
	char search_history[50];
	char genre[50];

	// Key word
	cout << "What is the name of the CS Field: ";
	cin.get(title, 50, '\n'); cin.ignore(100, '\n');

	// Definition
	cout << "Enter the definition: ";
	cin.get(desc, 50, '\n'); cin.ignore(100,'\n');

	// Three aspects
	cout << "Enter the three aspects: ";
	cin.get(aspects, 50, '\n'); cin.ignore(100,'\n');

	// Search history
	cout << "Enter the search history: ";
	cin.get(search_history, 50, '\n'); cin.ignore(100,'\n');

	// Genre 
	cout << "Enter the genre: ";
	cin.get(genre, 50, '\n'); cin.ignore(100,'\n');

	// create the cs field
	if (field.create_field(title, desc, aspects, search_history, genre)){
		cout << endl;
		cout << "Field successfully added" << endl;
		cout << endl;
	}
										
	// TODO test the hash function
	cout << "Add function" << endl;
	hash_table.insert(title, field);	
}

void load_ex_file(table & hash_table){
/*
	This file will load the external data file
*/

	switch (int num_loaded = hash_table.load()){
		case 0:
			cout << endl;
			cout << "There was an error loading the external file" << endl;
			cout << endl;
			break;
		default: 
			cout << endl;
			cout << num_loaded << " fields have been loaded!" << endl;
			cout << endl;
			break;

	}
}

void display_all(table & hash_table){
/*
	Display all data in the hash table
*/

	switch (hash_table.display_all()){
		case 0: 
		cout << endl;
		cout << "There was an error displaying" << endl;
		cout << endl;
		break;
	case 1: 
		cout << endl;
		cout << "All items displayed" << endl;
		cout << endl;
		break;
	}

}

void retrieve(table & hash_table){
	
	cs_field retrieve;	// Blank cs field
	
	char key_word[50];

	cout << "What CS Field would you like to look for: ";
	cin.get(key_word, 50, '\n'); cin.ignore(50, '\n');
	
	switch (hash_table.retrieve(key_word, retrieve)){
		case 0:
			cout << endl;
			cout << "No matching field was found" << endl;
			cout << endl;
			break;
		case 1:
			cout << endl;
			retrieve.display();
			cout << endl;
			break;


	}
	
}

void remove(table & hash_table){
	/*
		This function will remove a cs field
	*/
	char key_value[50];	
	cout << "What cs field would you like to delete: ";
	cin.get(key_value, 50, '\n'); cin.ignore(50, '\n'); 	
	
	switch(hash_table.remove(key_value)){
		case 0:
			cout << endl;
			cout << "This cs field does not exist!" << endl;
			cout << endl;
			break;
		case 1:
			cout << endl;
			cout << "Match delete"  << endl;
			cout << endl;
	}	
	
}
