#include <fstream> // This is for loading the file into the table
//#include "bigdata.txt"
#include "table.h"
/*
	 Hayden Chalin
	 CS163
	 5-7-2024
	 This file has the member functions for the table class
 */

table::table(int size){
	// Constructor
	hash_table = new node * [size];
	for (int i {0}; i < size; ++i)
		hash_table[i] = nullptr;
	table_size = size;

}

table::~table(void){
	/*
		 Destructor
		 XXX call the cs_field destructor for each node
	 */
	/* call a function here to loop through the chain and delete it
		 for (int i {0}; i < size: ++i){

}
	 */
	delete [] hash_table;
	hash_table = nullptr;

}

int table::hash(char * key_value){
/*
	This function will produce the hash value base off of the key_value
*/

	int hash {0};


	int long ascii_sum {0};
	int keylen = strlen(key_value);

	for (int i {0}; i < keylen; ++i)
		ascii_sum += tolower(key_value[i]);

	//hash = (ascii_sum * 997) % table_size;
	hash = (ascii_sum) % table_size;


	return hash;
}

int table::insert(char * key_value, const cs_field & to_add){
// This function adds it to the hash table

	int index = hash(key_value);
	if (!hash_table[index]){
		cout << "HASH: " << index << endl;
		cout << endl;
		hash_table[index] = new node;
		hash_table[index]->next = nullptr;
		hash_table[index]->field = to_add;
		return 1;
	}
	cout << "CHAINING" << endl;
	cout << "HASH: " << index << endl;
	node * new_node = new node;
	new_node->field = to_add;
	new_node->next = hash_table[index];
	hash_table[index] = new_node;


	cout << endl;

	return 1;
}

int table::retrieve(char * key_value, cs_field & found){
	/*
		Wrapper for the retriece function
	*/

	int i = hash(key_value);
	int chain_idx {0}; // used to see the position in the chain
	node * curr = hash_table[i];
	// traverse the chain
	while (curr){
		if (curr->field.key_matches(key_value)){
			cout << "Found at index: " << i << endl;
			cout << "Chain index: " << chain_idx << endl;
			found = curr->field; //FIXME This may be the incorrect way to do this???
			return 1;
		}
		++chain_idx;
		curr = curr->next;
	}
	return 0;

}

int table::display_all(void)const{
	/*
		Wrapper function for display all
		TODO Think of other base cases here
	*/
	if (!table_size)
		return 0;

	int i {0}; // Index of the hash array
	int count {0}; // Number of items displayed

	return display_all(i, ++count);
};

int table::display_all(int & i, int & count)const{
	/*
		recursive display function
	*/

	// if index > table_size, end of table: return
	// XXX ALWAYS CHECK THIS FIRST! XXX //
	if (i >= table_size)
		return 1; // all items displayed

	// if no head, go to next index
	if (!hash_table[i]) // same as if (!head)
		return display_all(++i, count);

	cout << "*********************" << endl;
	cout << "***** New Chain ***** " << endl;
	cout << "*********************" << endl;



	node * curr = hash_table[i];
	int chain_index {0};
	while (curr){
		// display the item
		cout << endl;
		cout << "**********" << endl;
		cout << "Hash number: " << i << endl;
		cout << "Chain Index: " << chain_index << endl;
		cout << "Count: " << count << endl;
		curr->field.display();
		cout << endl;
		++chain_index;
		++count;
		curr = curr->next;
	}

	return display_all(++i, count);
}

int table::load(void){
/*
	Load the external data file
*/

	const char ex_file_name[50] = "bigdata.txt";
	int count {0};
	ifstream in_file;
	in_file.open(ex_file_name);


	if(in_file){

		// XXX Uncomment this to only load 50, 100 items will not display in the stream XXX//
		//while(in_file && !in_file.eof() && count < 50){
		while(in_file && !in_file.eof()){
			cs_field field;

			char title[1000];
			char desc[1000];
			char aspects[1000];
			char search_history[1000];
			char genre[1000];

			// Title
			in_file.get(title, 1000, '|');
			in_file.ignore(1000, '|');

			// Description
			in_file.get(desc, 1000, '|');
			in_file.ignore(1000, '|');

			// Aspects
			in_file.get(aspects, 1000, '|');
			in_file.ignore(1000, '|');

			// Search history
			in_file.get(search_history, 1000, '|');
			in_file.ignore(1000, '|');

			// Genre
			in_file.get(genre, 1000, '\n');
			in_file.ignore(1000, '\n');


			// create the cs field
			field.create_field(title, desc, aspects, search_history, genre);
			insert(title, field);

			++count;
			cout << endl;
			cout << "COUNT: " << count << endl;
			field.display();
			cout << endl;
		}
		in_file.close();
	}

	cout << count << " Items have been loaded" << endl;
	return count;
};

int table::remove(char * key_value){
/*
	Remove cs field wrapper function
*/

	int key_idx = hash(key_value);

	if (!hash_table[key_idx]) // no head
		return 0;

	// Only one node and matches
	if (!hash_table[key_idx]->next && hash_table[key_idx]->field.key_matches(key_value)){
			hash_table[key_idx]->field.remove();
			delete hash_table[key_idx];
			hash_table[key_idx] = nullptr;
			return 1;
		}

	// First index matches
	if (hash_table[key_idx]->field.key_matches(key_value)){
		node * hold = hash_table[key_idx]->next;
		hash_table[key_idx]->field.remove();
		delete hash_table[key_idx];
		hash_table[key_idx] = hold;
		hold = nullptr;
		return 1;
	}

	node * head = hash_table[key_idx];
	return remove(head, key_value);

}
int table::remove(node * & head, char * key_value){
	/*
	This is the recursive call for remove */

	if (!head) return 1;

	if (head->field.key_matches(key_value)){

		node * hold = head->next;
		head->field.remove();
		delete head;
		head = hold;
		return 1;
	}

	return remove(head->next, key_value);
}
