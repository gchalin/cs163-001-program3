#include <cstring>
#include <iostream>
/*
	Hayden Chalin
	CS163
	5-7-2024
	This file will contain the class for the table ADT and member prototypes

*/
/*
	XXX This is what the data looks like:
	"Keyword|Description|List of three Aspects|Search History|something else\n"
*/

using namespace std;

// include guards
#ifndef FIELD
#define FIELD
class cs_field {
	public:
		cs_field();
		~cs_field();
		int display() const; // This will display the instances data
		int copy(cs_field & field);
		int create_field(char * title, char * definition, char * three_aspects, char * search_history, char * genre);
		bool key_matches(char * key_value);
		int remove(); 

	private:
		char * title;
		char * definition;
		char * three_aspects;
		char * search_history;
		char * genre;



};
#endif
