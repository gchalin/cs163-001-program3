#include "cs_field.h"
/*
	 Hayden Chalin
	 CS163
	 5-7-2024
	 This file has the member functions for the cs_field class
 */

cs_field::cs_field(){
	// constructor
	title = nullptr;
	definition = nullptr;
	three_aspects = nullptr;
	search_history = nullptr;
	genre = nullptr;
}

cs_field::~cs_field(){
	// desructor
	//XXX call this function when dealocating the tablel as well
}

int cs_field::create_field(char * temp_key_word, char * temp_def, char * temp_aspects, char * temp_history, char * temp_genre){
	/*
		This function will create an instance of the cs_field
		TODO get advice on how to optimize this
	*/
	title = new char[strlen(temp_key_word) + 1];
	strcpy(title, temp_key_word);
	
	definition = new char[strlen(temp_def) + 1];
	strcpy(definition, temp_def);

	three_aspects = new char[strlen(temp_aspects) + 1];
	strcpy(three_aspects, temp_aspects);

	search_history = new char[strlen(temp_history) + 1];
	strcpy(search_history, temp_history);

	genre = new char[strlen(temp_genre) + 1];
	strcpy(genre, temp_genre);
	
	return 1;
}

int cs_field::display()const{
	// This funciton will display one cs field
	cout << "Key word: " << title << endl;
	cout << "Definition: " << definition << endl;
	cout << "Three aspects: " << three_aspects << endl;
	cout << "Search history: " << search_history << endl;
	cout << "Genre: " << genre << endl;
	
	return 1;
}


bool cs_field::key_matches(char * key_value){
	// compares title to key passed in
	if (strcmp(title, key_value) == 0)
		return 1;
	else
		return 0;

}

int cs_field::remove(){
		
	delete [] title;
	title = nullptr;
	delete [] definition;
	definition = nullptr;
	delete [] three_aspects;
	three_aspects = nullptr;
	delete [] search_history;
	search_history = nullptr;
	delete [] genre;
	genre = nullptr;
	//delete cs_field;
	return 1;

}
